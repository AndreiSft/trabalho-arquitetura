package br.ucsal.TrabalhoArquitetura.service;

import org.springframework.http.ResponseEntity;

import br.ucsal.TrabalhoArquitetura.model.Aluno;

public interface AlunoService {
	
	ResponseEntity<?> cadastrar(Aluno aluno);
	
}
