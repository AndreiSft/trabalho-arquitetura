package br.ucsal.TrabalhoArquitetura.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import br.ucsal.TrabalhoArquitetura.model.Aluno;
import br.ucsal.TrabalhoArquitetura.model.Aula;
import br.ucsal.TrabalhoArquitetura.model.Turma;

public interface TurmaService {
	
	ResponseEntity<?> cadastrar(Turma turma);
	ResponseEntity<?> listar();
	ResponseEntity<?> adicionarAluno(List<Aluno> aluno, Long turmaId);
	ResponseEntity<?> cadastrarCronograma(List<Aula> aulas, Long cronogramaId);
	
}
