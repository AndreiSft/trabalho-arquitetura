package br.ucsal.TrabalhoArquitetura.service;

import org.springframework.http.ResponseEntity;

import br.ucsal.TrabalhoArquitetura.model.Docente;

public interface DocenteService {
	
	ResponseEntity<?> cadastrar(Docente docente);
	
}
