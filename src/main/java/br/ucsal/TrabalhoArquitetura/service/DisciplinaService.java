package br.ucsal.TrabalhoArquitetura.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import br.ucsal.TrabalhoArquitetura.model.Assunto;
import br.ucsal.TrabalhoArquitetura.model.Disciplina;

public interface DisciplinaService {
	
	ResponseEntity<?> cadastrar(Disciplina disciplina);
	ResponseEntity<?> adicionarAssunto(List<Assunto> assuntos, Long disciplinaId);
	
}
