package br.ucsal.TrabalhoArquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.TrabalhoArquitetura.model.Docente;

@Repository
public interface DocenteRepository extends JpaRepository<Docente, Long> {

}
