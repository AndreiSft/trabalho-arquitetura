package br.ucsal.TrabalhoArquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.TrabalhoArquitetura.model.Aula;

@Repository
public interface AulaRepository extends JpaRepository<Aula, Long> {
	
}
