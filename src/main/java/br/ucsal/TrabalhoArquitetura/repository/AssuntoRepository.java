package br.ucsal.TrabalhoArquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.TrabalhoArquitetura.model.Assunto;

@Repository
public interface AssuntoRepository extends JpaRepository<Assunto, Long> {
	
}
