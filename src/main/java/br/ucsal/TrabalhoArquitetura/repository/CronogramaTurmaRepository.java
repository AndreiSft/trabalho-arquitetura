package br.ucsal.TrabalhoArquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ucsal.TrabalhoArquitetura.model.CronogramaTurma;

@Repository
public interface CronogramaTurmaRepository extends JpaRepository<CronogramaTurma, Long> {

}
