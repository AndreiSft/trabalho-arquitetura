package br.ucsal.TrabalhoArquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ucsal.TrabalhoArquitetura.model.Turma;

public interface TurmaRepository extends JpaRepository<Turma, Long> {

	Turma getById(Long id);
	
}
