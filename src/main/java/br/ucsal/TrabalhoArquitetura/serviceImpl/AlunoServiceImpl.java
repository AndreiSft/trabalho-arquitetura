package br.ucsal.TrabalhoArquitetura.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.ucsal.TrabalhoArquitetura.model.Aluno;
import br.ucsal.TrabalhoArquitetura.repository.AlunoRepository;
import br.ucsal.TrabalhoArquitetura.service.AlunoService;

@Service
public class AlunoServiceImpl implements AlunoService {

	@Autowired
	AlunoRepository alunoRepository;
	
	@Override
	public ResponseEntity<?> cadastrar(Aluno aluno) {
		try {
			alunoRepository.saveAndFlush(aluno);
			return new ResponseEntity<String>("Aluno cadastrado com sucesso.", HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Ocorreu um erro ao tentar realizar o cadastro.", HttpStatus.BAD_REQUEST);
		}
	}

}
