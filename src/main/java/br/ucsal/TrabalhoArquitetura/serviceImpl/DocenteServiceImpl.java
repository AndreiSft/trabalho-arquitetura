package br.ucsal.TrabalhoArquitetura.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.ucsal.TrabalhoArquitetura.model.Docente;
import br.ucsal.TrabalhoArquitetura.repository.DocenteRepository;
import br.ucsal.TrabalhoArquitetura.service.DocenteService;

@Service
public class DocenteServiceImpl implements DocenteService {

	@Autowired
	DocenteRepository docenteRepository;
	
	@Override
	public ResponseEntity<?> cadastrar(Docente docente) {
		try {
			docenteRepository.saveAndFlush(docente);
			return new ResponseEntity<String>("Docente cadastrado com sucesso.", HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Ocorreu um erro ao tentar realizar o cadastro.", HttpStatus.BAD_REQUEST);
		}
	}

}
