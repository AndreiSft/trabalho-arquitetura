package br.ucsal.TrabalhoArquitetura.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.ucsal.TrabalhoArquitetura.model.Aluno;
import br.ucsal.TrabalhoArquitetura.model.Aula;
import br.ucsal.TrabalhoArquitetura.model.CronogramaTurma;
import br.ucsal.TrabalhoArquitetura.model.Turma;
import br.ucsal.TrabalhoArquitetura.repository.AlunoRepository;
import br.ucsal.TrabalhoArquitetura.repository.AulaRepository;
import br.ucsal.TrabalhoArquitetura.repository.CronogramaTurmaRepository;
import br.ucsal.TrabalhoArquitetura.repository.DisciplinaRepository;
import br.ucsal.TrabalhoArquitetura.repository.TurmaRepository;
import br.ucsal.TrabalhoArquitetura.service.TurmaService;

@Service
public class TurmaServiceImpl implements TurmaService {

	@Autowired
	TurmaRepository turmaRepository;
	
	@Autowired
	AlunoRepository alunoRepository;
	
	@Autowired
	CronogramaTurmaRepository cronogramaTurmaRepository;
	
	@Autowired 
	AulaRepository aulaRepository;
	
	@Autowired
	DisciplinaRepository disciplinaRepository;
	
	@Override
	public ResponseEntity<?> cadastrar(Turma turma) {
		try {
			turmaRepository.saveAndFlush(turma);
			return new ResponseEntity<String>("Turma cadastrada com sucesso", HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Ocorreu um erro ao tentar cadastrar a turma", HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<?> listar() {
		return new ResponseEntity<List<Turma>>(turmaRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> adicionarAluno(List<Aluno> alunos, Long turmaId) {
		try {
			if (turmaRepository.existsById(turmaId)) {
				for (Aluno aluno : alunos) {
					Aluno alunoDB = alunoRepository.getByMatricula(aluno.getMatricula());
					if (alunoDB != null) {						
						alunoDB.setTurma(turmaRepository.getById(turmaId));
						alunoRepository.saveAndFlush(alunoDB);
					} else {
						return new ResponseEntity<String>("Aluno não encontrado.", HttpStatus.BAD_REQUEST);
					}
				}
				return new ResponseEntity<String>("Aluno(s) adicionado(s) à turma com sucesso.", HttpStatus.CREATED);
			} else {
				return new ResponseEntity<String>("Turma não encontrada.", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<String>("Ocorreu um erro ao tentar realizar o cadastro.", HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<?> cadastrarCronograma(List<Aula> aulas, Long cronogramaId) {
		try {			
			if (turmaRepository.existsById(cronogramaId)) {
				CronogramaTurma cronogramaTurma = new CronogramaTurma();
				cronogramaTurma.setTurma(turmaRepository.getById(cronogramaId));
				cronogramaTurmaRepository.saveAndFlush(cronogramaTurma);
				
				for (Aula aula : aulas) {
					aula.setCronogramaTurma(cronogramaTurma);
					aulaRepository.saveAndFlush(aula);
				}
				
				return new ResponseEntity<String>("CronogramaTurma cadastrado com sucesso.", HttpStatus.CREATED);
			} else {
				return new ResponseEntity<String>("Turma não encontrada.", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<String>("Ocorreu um erro ao tentar realizar o cadastro.", HttpStatus.BAD_REQUEST);
		}
	}

}
