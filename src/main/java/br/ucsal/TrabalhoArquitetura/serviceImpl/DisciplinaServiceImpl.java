package br.ucsal.TrabalhoArquitetura.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.ucsal.TrabalhoArquitetura.model.Assunto;
import br.ucsal.TrabalhoArquitetura.model.Disciplina;
import br.ucsal.TrabalhoArquitetura.repository.AssuntoRepository;
import br.ucsal.TrabalhoArquitetura.repository.DisciplinaRepository;
import br.ucsal.TrabalhoArquitetura.service.DisciplinaService;

@Service
public class DisciplinaServiceImpl implements DisciplinaService {

	@Autowired
	DisciplinaRepository disciplinaRepository;

	@Autowired
	AssuntoRepository assuntoRepository;
	
	@Override
	public ResponseEntity<?> cadastrar(Disciplina disciplina) {
		try {
			disciplinaRepository.saveAndFlush(disciplina);
			return new ResponseEntity<String>("Disciplina cadastrada com sucesso.", HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Ocorreu um erro ao tentar realizar o cadastro.", HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<?> adicionarAssunto(List<Assunto> assuntos, Long disciplinaId) {

		try {
			if (disciplinaRepository.existsById(disciplinaId)) {
				for (Assunto assunto : assuntos) {
					Assunto a = new Assunto();
					a.setNome(assunto.getNome());
					a.setDisciplina(disciplinaRepository.getById(disciplinaId));
					assuntoRepository.save(a);
				}
				return new ResponseEntity<String>("Assunto(s) cadastrado(s) com sucesso.", HttpStatus.CREATED);
			} else {
				return new ResponseEntity<String>("Disciplina não encontrada.", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<String>("Ocorreu um erro ao tentar realizar o cadastro.", HttpStatus.BAD_REQUEST);
		}
	}

}
