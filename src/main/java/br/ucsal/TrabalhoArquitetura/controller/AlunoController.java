package br.ucsal.TrabalhoArquitetura.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ucsal.TrabalhoArquitetura.model.Aluno;
import br.ucsal.TrabalhoArquitetura.service.AlunoService;

@RestController
@RequestMapping("/aluno")
public class AlunoController {

	@Autowired
	AlunoService alunoService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<?> cadastrar(@RequestBody @Valid Aluno aluno) {
		return alunoService.cadastrar(aluno);
	}
	
}
