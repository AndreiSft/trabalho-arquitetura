package br.ucsal.TrabalhoArquitetura.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ucsal.TrabalhoArquitetura.model.Assunto;
import br.ucsal.TrabalhoArquitetura.model.Disciplina;
import br.ucsal.TrabalhoArquitetura.service.DisciplinaService;

@RestController
@RequestMapping("/disciplina")
public class DisciplinaController {
	
	@Autowired
	DisciplinaService disciplinaService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<?> cadastrar(@RequestBody @Valid Disciplina disciplina) {
		return disciplinaService.cadastrar(disciplina);
	}
	
	@PostMapping("/adicionar-assunto/{disciplinaId}")
	public ResponseEntity<?> adicionarAssunto(@RequestBody @Valid List<Assunto> assuntos, @PathVariable Long disciplinaId) {
		return disciplinaService.adicionarAssunto(assuntos, disciplinaId);
	}
	
}