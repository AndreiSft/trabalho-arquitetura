package br.ucsal.TrabalhoArquitetura.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ucsal.TrabalhoArquitetura.model.Aluno;
import br.ucsal.TrabalhoArquitetura.model.Aula;
import br.ucsal.TrabalhoArquitetura.model.Turma;
import br.ucsal.TrabalhoArquitetura.service.AlunoService;
import br.ucsal.TrabalhoArquitetura.service.TurmaService;

@RestController
@RequestMapping("/turma")
public class TurmaController {

	@Autowired
	TurmaService turmaService;
	
	@Autowired
	AlunoService alunoService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<?> cadastrar(@RequestBody @Valid Turma turma) {
		return turmaService.cadastrar(turma);
	}
	
	@PostMapping("/cadastrar-cronograma/{cronogramaId}")
	public ResponseEntity<?> cadastrarCronograma(@RequestBody @Valid List<Aula> aulas, @PathVariable Long cronogramaId) {
		return turmaService.cadastrarCronograma(aulas, cronogramaId);
	}
	
	@PutMapping("/adicionar-aluno/{turmaId}")
	public ResponseEntity<?> adicionarAluno(@RequestBody @Valid List<Aluno> alunos, @PathVariable Long turmaId) {
		return turmaService.adicionarAluno(alunos, turmaId);
	}
	
	@GetMapping("/turmas")
	public ResponseEntity<?> listar() {
		return turmaService.listar();
	}
	
}
