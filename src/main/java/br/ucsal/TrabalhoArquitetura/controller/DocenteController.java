package br.ucsal.TrabalhoArquitetura.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ucsal.TrabalhoArquitetura.model.Docente;
import br.ucsal.TrabalhoArquitetura.service.DocenteService;

@RestController
@RequestMapping("/docente")
public class DocenteController {
	
	@Autowired
	DocenteService docenteService;
	
	@PostMapping("/cadastrar")
	public ResponseEntity<?> cadastrar(@RequestBody @Valid Docente docente) {
		return docenteService.cadastrar(docente);
	}
	

}
