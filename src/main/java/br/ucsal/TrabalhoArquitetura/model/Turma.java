package br.ucsal.TrabalhoArquitetura.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;

@Entity
public class Turma implements Serializable {
	
	private static final long serialVersionUID = -3330904463515621954L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank
	private String nome;

	@OneToMany(mappedBy = "turma", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Aluno> alunos;

	@OneToOne(mappedBy = "turma", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	private CronogramaTurma cronogramaTurma;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public CronogramaTurma getCronogramaTurma() {
		return cronogramaTurma;
	}

	public void setCronogramaTurma(CronogramaTurma cronogramaTurma) {
		this.cronogramaTurma = cronogramaTurma;
	}
	

}
