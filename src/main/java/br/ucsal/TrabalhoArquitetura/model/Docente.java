package br.ucsal.TrabalhoArquitetura.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "pessoa_id")
public class Docente extends Pessoa implements Serializable {
	
	private static final long serialVersionUID = 8770328019421206308L;

}
