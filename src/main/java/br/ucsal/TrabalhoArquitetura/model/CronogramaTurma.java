package br.ucsal.TrabalhoArquitetura.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class CronogramaTurma implements Serializable {
	
	private static final long serialVersionUID = -1955340337307207910L;

	@Id
	private Long id;
	
	@OneToMany(mappedBy = "cronogramaTurma", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Aula> aulas;
	
	@OneToOne(fetch = FetchType.LAZY)
    @MapsId
	private Turma turma;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Aula> getAulas() {
		return aulas;
	}

	public void setAulas(List<Aula> aulas) {
		this.aulas = aulas;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

}
